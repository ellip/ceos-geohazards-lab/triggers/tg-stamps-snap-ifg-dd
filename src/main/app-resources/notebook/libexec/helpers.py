import cioppy
from shapely.wkt import loads
#import geopandas as gp    
import pandas as pd
import os
#import glob
import sys
#import gdal
import datetime as dt

def get_product_metadata(input_references, fields='identifier,self,wkt,startdate,enddate,enclosure,track'):
    
    ciop = cioppy.Cioppy()

    if isinstance(input_references, str):

        search_params = dict()

        search_params['do'] = 'terradue'

        products = pd.DataFrame(ciop.search(end_point=input_references, 
                            params=search_params,
                            output_fields=fields,
                            model='EOP'))

    else:    

        temp_results = []

        for index, self in enumerate(input_references):

            search_params = dict()

            search_params['do'] = 'terradue'

            temp_results.append(ciop.search(end_point=self, 
                                params=search_params,
                                output_fields=fields, 
                                model='EOP')[0])

        products = pd.DataFrame(temp_results)
    
    if 'startdate' in fields:
        products.startdate = pd.to_datetime(products.startdate)
        #products.startdate = pd.to_datetime(products.startdate).astype(dt.datetime)
    
    if 'enddate' in fields:
        products.enddate = pd.to_datetime(products.enddate)
        #products.enddate = pd.to_datetime(products.enddate).astype(dt.datetime)
        
    if 'wkt' in fields:
        products = products.rename(index=str, columns={'wkt': 'geometry'})
        products['geometry'] = products['geometry'].apply(loads)
        
    
    #products = products.merge(products.apply(lambda row: update(row, data_path), axis=1), 
    #                      left_index=True,
    #                      right_index=True)
    return products 

def ms_intersection(row, master):
    
    return (row['geometry'].intersection(master.geometry.values[0]).area/master.geometry.values[0].area) * 100

def is_first_run(row, first_slave):

    if row.self == first_slave:
        first_run = 'Yes'
    else:
        first_run = 'No'
        
    return first_run


def create_date(row):
    series = dict()
    series['day']=str(row['startdate'])[:10]


    return pd.Series(series)