import datetime
import owslib
from owslib.wps import monitorExecution
from owslib.wps import WebProcessingService
import time
import lxml.etree as etree
import json
import hashlib
from datetime import datetime
import datetime
import dateutil.parser
from shapely.wkt import loads
import cioppy

import pandas as pd
from jinja2 import Template


def is_date_valid(date_text):
    """This function checks the data format against the pattern '%Y-%m-%dT%H:%MZ'
    
    Args:
        date_text: the date as string.
        
    Returns
        True if the date is in the format '%Y-%m-%dT%H:%MZ'.
    
    Raises:
        ValueError.
    """
    try:
        datetime.datetime.strptime(date_text, '%Y-%m-%dT%H:%MZ')
    except ValueError:
        raise ValueError('Incorrect data format, should be YYYY-MM-DDTHH:MMZ')
        
    return True

def list_parameters(wps_url, process_id, verbose=False):
    """This function prints the human readable WPS parameters for the provided WPS process identifier and returns a dictionary with the parameters as keys (no default values) 
    
    Args:
        wps_url: the WPS end-point.
        process_id: the process identifier
        verbose: print the human readable WPS parameters
        
    Returns
        A dictionary with the parameters as keys (no default values) .
    
    Raises:
        None.
    """
    wps = WebProcessingService(wps_url, verbose=False, skip_caps=True)
    
    process = wps.describeprocess(process_id)

    data_inputs = dict()
    for data_input in process.dataInputs:

        if data_input.identifier == '_T2Username' or data_input.identifier == '_T2ApiKey': 
            continue

        if (data_input.minOccurs != 0) or (data_input.identifier == 't2_coordinator_name'):

                data_inputs[data_input.identifier] = ''
                if verbose:
                    print ('Parameter identifier: {0}\n'.format(data_input.identifier))
                    print ('Title/abstract: {0} - {1}\n'.format(data_input.title, data_input.abstract)) 
            
    return data_inputs


            
    return data_inputs

def is_process_deployed(wps_url, process_id):
    """This function returns True if the process is deployed
    
    Args:
        wps_url: the WPS end-point.
        process_id: the process identifier
        
    Returns
        True if the process is deployed.
    
    Raises:
        ValueError.
    """
    
    wps = WebProcessingService(wps_url, verbose=False, skip_caps=True)

    wps.getcapabilities()

    deployed = False

    for index, elem in enumerate(wps.processes):

        if process_id in elem.identifier:
            deployed = True
            print ('Process {0} is deployed'.format(process_id))

    if not deployed:

        raise ValueError('Process {0} not deployed'.format(process_id))
    
    return True


def get_parameter_string(wps_url, process_id):
    """This function returns a string with the WPS parameters and their associated default values 
    
    Args:
        wps_url: the WPS end-point.
        process_id: the process identifier
        
    Returns
        A string with the parameters and their associated default values.
    
    Raises:
        None.
    """
    
    
    wps = WebProcessingService(wps_url, verbose=False, skip_caps=True)
    
    process = wps.describeprocess(process_id)

    data_inputs = []

    for data_input in process.dataInputs:
        # after TRIGGER-15
        if data_input.identifier == '_T2Username' or data_input.identifier == '_T2ApiKey': continue
        
        if (data_input.minOccurs != 0) or (data_input.identifier == 't2_coordinator_name') or (data_input.identifier == '_T2Username'):

            data_inputs.append('{}={}'.format(data_input.identifier, str(data_input.defaultValue).replace(',', '|')))

    return ','.join(data_inputs)
    
    
    
    
def get_acquistions(search_params, data_pipeline_parameters):
    """This function does a search using the search_params
    
    Args:
        search_params: a dictionary with the search parameters. It's values for the keys start, stop and update are updated with the values of the data_pipeline_parameters keys start, stop and update
        data_pipeline_parameters: a dictionary with at least the keys 'osd', 'username' and 'apikey' for the OpenSearch end-point
        
    Returns
        geopandas dataframe.
    
    Raises:
        None.
    """
    
    
    context = dict()

    for key in ['start', 'stop', 'update']:

        if key in data_pipeline_parameters.keys():    
            search_params[key] = data_pipeline_parameters[key]

    print(search_params)
    

    ciop = cioppy.Cioppy()

    fields = 'identifier,self,wkt,startdate,enddate,enclosure,track,productType,swathIdentifier,cc'

    search = pd.DataFrame(ciop.search(end_point=data_pipeline_parameters['osd'],
                                         params=search_params,
                                         output_fields=fields,
                                         timeout=search_params['timeout'] if 'timeout' in search_params.keys() else 20000,
                                         creds='{}:{}'.format(data_pipeline_parameters['username'], data_pipeline_parameters['api_key']),
                                         all_enclosures=True if 'all_enclosures' in search_params.keys() else False,
                                         model='EOP')) 

    if len(search) == 0:
        
        return search
        
    if 'startdate' in fields:
        search.startdate = pd.to_datetime(search.startdate) #.astype(dt.datetime)

    if 'enddate' in fields:
        search.enddate = pd.to_datetime(search.enddate) #.astype(dt.datetime)

    if 'wkt' in fields:
        search = search.rename(index=str, columns={'wkt': 'geometry'})
        search['geometry'] = search['geometry'].apply(loads)
 
    return search



def analyse_geometry(row, aoi):

    series = dict()
    
    series['intersection_percentage'] = (row['geometry'].intersection(loads(aoi)).area/loads(aoi).area)*100

    return pd.Series(series)


def create_coordinator_di(inputs, trigger):
    """This function create a coordinator data item
    
    Args:
        inputs: a dictionary with the coordinator parameters parameters with at least the keys: t2_coordinator_date_start, t2_coordinator_date_stop, t2_coordinator_name
        trigger: the trigger instance associated to the data pipeline.
        
    Returns
        The coordinator data item.
    
    Raises:
        None.
    """
    
    
    is_date_valid(inputs['t2_coordinator_date_start'])
    is_date_valid(inputs['t2_coordinator_date_stop'])
        
    coordinator_di = trigger.create_empty_data_item()

    # set the base metadata
    coordinator_di.set_title(inputs['t2_coordinator_name'])
    

    m = hashlib.md5()
    
    m.update(str(''.join([''.join(inputs.values()), 
                      trigger.data_pipeline, 
                      trigger.wps_url, 
                      trigger.process_id])).encode('utf-8'))

    identifier = m.hexdigest()

    coordinator_di.set_identifier(identifier)

    # todo add to triggers: handle date_end as foreseen in trigger code
    coordinator_di.set_date(dateutil.parser.parse(inputs['t2_coordinator_date_start']),
                            dateutil.parser.parse(inputs['t2_coordinator_date_stop']))

    coordinator_di.set_geometry(loads('POINT(0 0)'))

    # set the category to ease the search
    coordinator_di.set_category("coordinator", 'coordinator', )

    coordinator_di.set_published(datetime.datetime.now())

    for key, value in inputs.items():
 
        coordinator_di.processing_parameters.append((key, value)) 
      
    coordinator_di.set_output('coordinatorIds')

    return coordinator_di

def is_date_valid(date_text):
    """This function checks the data format against the pattern '%Y-%m-%dT%H:%MZ'
    
    Args:
        date_text: the date as string.
        
    Returns
        True if the date is in the format '%Y-%m-%dT%H:%MZ'.
    
    Raises:
        ValueError.
    """
    try:
        datetime.datetime.strptime(date_text, '%Y-%m-%dT%H:%MZ')
    except ValueError:
        raise ValueError('Incorrect data format, should be YYYY-MM-DDTHH:MMZ')
        
    return True